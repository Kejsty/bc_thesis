#include <pthread.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <pthread.h>

const char *message = "1234567";

void *worker( void *_ ) {
    
    char buf[ 8 ] = {};
    int fd = open( "exampleFile", O_RDONLY );
    assert( fd >= 0 );

    char incoming;
    int red = 0;
    while (1) {
        while (read( fd, &incoming, 1 ) == 0) {}
        if ( incoming == '-' )
            break;
        buf[ red ] = incoming;
        ++red;
    }

    assert( strcmp( buf, message ) == 0 );
    assert( close( fd ) == 0 );
    return _;
}

int main() {

    int fd = open( "exampleFile",  O_WRONLY | O_CREAT, 0644);
    assert( fd >= 0 );

    pthread_t thread;
    pthread_create( &thread, NULL, worker, NULL );

    assert( write( fd, message, 7 ) == 7 );
    assert( write( fd, "-", 1 ) == 1 );
    assert( close( fd ) == 0 );
  
    pthread_join( thread, NULL );
    return 0;
}

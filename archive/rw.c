#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdint.h>

int main() {
    char buf[12] = {};

    int fd = open( "file47", O_WRONLY | O_CREAT, 0644 );
    assert( fd >= 0 );

    assert( write( fd, "passthrough", 11 ) == 11 );
    assert( close( fd ) == 0 );

    int fd = open( "file47", O_RDONLY );
    assert( fd >= 0 );

    int res = read( fd, buf, 11 );
    assert(  res == 11 );
    assert( strcmp( "passthrough", buf) == 0 );

    assert( close( fd ) == 0 );
    return 0;
}
